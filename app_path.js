const path = require('path');

const appPaths = {
  src: path.join(__dirname, 'src'),
  assets: path.join(__dirname, 'src', 'assets'),
  dist: path.join(__dirname, 'dist')
};

module.exports = appPaths;
