const dev = require('./webpack.dev.config');

module.exports = function(env, args) {
  return process.env.NODE_ENV === "development" ? dev : dev;
};
