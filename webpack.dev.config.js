const path = require('path');
const appPaths = require('./app_path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.config');


const dev = {

  entry: [
    path.join(appPaths.assets, 'index.js')
  ],

  mode: 'development',
  devtool: 'source-map',

  devServer: {
    hot: true,
    stats: { colors: true },
    contentBase: appPaths.dist
  },

  module: {
    rules: [{
      test: /\.elm$/,
      exclude: [/elm-stuff/, /node_modules/],
      use: {
        loader: 'elm-webpack-loader',
        options: {
          debug: true,
          verbose: true
        }
      }
    }]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]

};

module.exports = merge(common, dev);
