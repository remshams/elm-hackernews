module Main exposing (Model, Msg(..), Route(..), initCmd, main, parseRoute, update, view)

import Browser
import Browser.Navigation as Nav
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)
import TopStories
import Url
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s)


main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }


type Route
    = TopStories
    | Comments


type alias Model =
    { route : Route
    , topStories : TopStories.Model
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        route =
            parseRoute url
    in
    ( Model route TopStories.init, initCmd route )


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | TopStoriesMsg TopStories.Msg


parseRoute : Url.Url -> Route
parseRoute =
    let
        routeParser =
            oneOf
                [ map TopStories (s "topStories")
                , map Comments (s "comments")
                ]
    in
    \url ->
        Maybe.withDefault TopStories (parse routeParser url)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            ( model, Cmd.none )

        UrlChanged url ->
            let
                route =
                    parseRoute url
            in
            ( { model | route = route }, initCmd route )

        TopStoriesMsg topStoriesMessage ->
            let
                ( newModel, command ) =
                    TopStories.update topStoriesMessage model.topStories
            in
            ( { model | topStories = newModel }, Cmd.map TopStoriesMsg command )


initCmd : Route -> Cmd Msg
initCmd route =
    case route of
        TopStories ->
            Cmd.map TopStoriesMsg TopStories.initCmd

        Comments ->
            Cmd.none


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


view : Model -> Browser.Document Msg
view model =
    { title = "Hackernews"
    , body =
        [ case model.route of
            TopStories ->
                TopStories.view model.topStories

            Comments ->
                viewComments model
        ]
    }


viewComments : Model -> Html msg
viewComments model =
    div []
        [ text "comments" ]
