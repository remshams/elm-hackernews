module Common.Rest exposing (baseUrl, baseUrlWithVersion, version)


baseUrl : String
baseUrl =
    "https://hacker-news.firebaseio.com"


baseUrlWithVersion : String
baseUrlWithVersion =
    baseUrl ++ "/v0"


version : String
version =
    "v0"
