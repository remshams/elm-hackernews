module TopStories exposing (Model(..), Msg(..), Story, StoryId(..), addStoryDetails, getStory, getStoryUrl, getTopStoryIds, getTopStoryIdsUrl, init, initCmd, loadStoryDetails, sameStory, storyDecoder, storyIdDecoder, storyPath, timestampDecoder, topStoryIdsDecoder, topStoryIdsPath, update, view, viewStory)

import Common.Rest
import Html exposing (Html, div, h1, li, text, ul)
import Html.Attributes exposing (class)
import Http
import Json.Decode as Decode
import Time
import Url.Builder as Url


topStoryIdsPath : String
topStoryIdsPath =
    "topstories.json"


storyPath : String
storyPath =
    "item"


type StoryId
    = StoryId String


type alias Story =
    { id : StoryId
    , title : String
    , time : Time.Posix
    , score : Int
    , url : String
    }


type Model
    = Loading
    | Loaded (List Story)
    | Failed Http.Error


type Msg
    = Load
    | LoadedStoryIds (Result Http.Error (List StoryId))
    | LoadedStory (Result Http.Error Story)


init : Model
init =
    Loading


initCmd : Cmd Msg
initCmd =
    getTopStoryIds


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Load ->
            ( Loading, getTopStoryIds )

        LoadedStoryIds loadIdResult ->
            case loadIdResult of
                Result.Ok storyIds ->
                    ( model, Cmd.batch (List.take 10 storyIds |> loadStoryDetails) )

                Result.Err error ->
                    ( Failed error, Cmd.none )

        LoadedStory loadResult ->
            case loadResult of
                Result.Ok loadedStory ->
                    case model of
                        Loading ->
                            ( Loaded [ loadedStory ], Cmd.none )

                        Loaded stories ->
                            ( Loaded (addStoryDetails stories loadedStory), Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                Result.Err error ->
                    ( Failed error, Cmd.none )


view : Model -> Html msg
view model =
    div []
        [ h1 [ class "f1", class "ml2" ] [ text "Top Stories" ]
        , case model of
            Loading ->
                div [] [ text "Loading" ]

            Loaded stories ->
                ul [] (List.map viewStory stories)

            Failed error ->
                div [] [ text "Error" ]
        ]


addStoryDetails : List Story -> Story -> List Story
addStoryDetails stories story =
    stories
        |> List.filter (sameStory story >> not)
        |> (::) story


sameStory : Story -> Story -> Bool
sameStory base compare =
    base.id == compare.id


viewStory : Story -> Html msg
viewStory story =
    li [] [ text story.title ]



-- HTTP


getTopStoryIds : Cmd Msg
getTopStoryIds =
    Http.send LoadedStoryIds (Http.get getTopStoryIdsUrl topStoryIdsDecoder)


getTopStoryIdsUrl : String
getTopStoryIdsUrl =
    Url.crossOrigin Common.Rest.baseUrlWithVersion [ topStoryIdsPath ] []


getStory : StoryId -> Cmd Msg
getStory storyId =
    Http.send LoadedStory (Http.get (getStoryUrl storyId) storyDecoder)


getStoryUrl : StoryId -> String
getStoryUrl (StoryId storyId) =
    Url.crossOrigin Common.Rest.baseUrlWithVersion [ storyPath, storyId ++ ".json" ] []


loadStoryDetails : List StoryId -> List (Cmd Msg)
loadStoryDetails storyIds =
    List.map (\storyId -> getStory storyId) storyIds



-- Decoders


topStoryIdsDecoder : Decode.Decoder (List StoryId)
topStoryIdsDecoder =
    Decode.list storyIdDecoder


storyDecoder : Decode.Decoder Story
storyDecoder =
    Decode.map5 Story
        (Decode.field "id" storyIdDecoder)
        (Decode.field "title" Decode.string)
        (Decode.field "time" timestampDecoder)
        (Decode.field "score" Decode.int)
        (Decode.field "url" Decode.string)


timestampDecoder : Decode.Decoder Time.Posix
timestampDecoder =
    Decode.map Time.millisToPosix Decode.int


storyIdDecoder : Decode.Decoder StoryId
storyIdDecoder =
    Decode.map (String.fromInt >> StoryId) Decode.int
