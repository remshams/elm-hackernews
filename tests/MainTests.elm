module MainTests exposing (defaultUrl, parseRoute, suite)

import Expect
import Main
import Test exposing (Test, describe, test)
import TopStories
import Url


defaultUrl : Url.Url
defaultUrl =
    { protocol = Url.Http
    , host = "localhost"
    , port_ = Just 8080
    , path = "/"
    , query = Nothing
    , fragment = Nothing
    }


defaultModel : Main.Model
defaultModel =
    Main.Model Main.Comments TopStories.init


suite : Test
suite =
    describe "Main"
        [ parseRoute
        , update
        , initCmd
        ]


parseRoute : Test
parseRoute =
    describe "parseRoute"
        [ test "parses topStories in case no other route matches" <|
            \_ ->
                let
                    url =
                        Url.fromString "http://localhost:8080/" |> Maybe.withDefault defaultUrl

                    routeExpected =
                        Main.TopStories

                    other =
                        "test"
                in
                Expect.equal (Main.parseRoute url) routeExpected
        , test "parses top stories" <|
            \_ ->
                let
                    url =
                        Url.fromString "http://localhost:8080/topStories" |> Maybe.withDefault defaultUrl

                    routeExpected =
                        Main.TopStories
                in
                Expect.equal (Main.parseRoute url) routeExpected
        , test "parses comments" <|
            \_ ->
                let
                    url =
                        Url.fromString "http://localhost:8080/comments" |> Maybe.withDefault defaultUrl

                    routeExpected =
                        Main.Comments
                in
                Expect.equal (Main.parseRoute url) routeExpected
        ]


update : Test
update =
    describe "update"
        [ test "updates route on url change" <|
            \_ ->
                let
                    msg =
                        Main.UrlChanged (Url.fromString "http://localhost:8080/topStories" |> Maybe.withDefault defaultUrl)

                    updateResultExpected =
                        ( { defaultModel | route = Main.TopStories }, Main.initCmd Main.TopStories )
                in
                Expect.equal (Main.update msg defaultModel) updateResultExpected
        , test "updates topStories model on topStories messages" <|
            \_ ->
                let
                    topStoriesMessage =
                        Main.TopStoriesMsg TopStories.Load

                    updateReturned =
                        let
                            ( topStoriesModel, topStoriesCommand ) =
                                TopStories.update TopStories.Load defaultModel.topStories
                        in
                        ( { defaultModel | topStories = topStoriesModel }, Cmd.map Main.TopStoriesMsg topStoriesCommand )
                in
                Expect.equal (Main.update topStoriesMessage defaultModel) updateReturned
        ]


initCmd : Test
initCmd =
    describe "InitCmd"
        [ test "inits topStories command" <|
            \_ ->
                let
                    route =
                        Main.TopStories

                    commandExpected =
                        Cmd.map Main.TopStoriesMsg TopStories.initCmd
                in
                Expect.equal (Main.initCmd route) commandExpected
        , test "inits comments command" <|
            \_ ->
                let
                    route =
                        Main.Comments

                    commandExpected =
                        Cmd.none
                in
                Expect.equal (Main.initCmd route) commandExpected
        ]
