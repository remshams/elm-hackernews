module TopStoriesTests exposing (init, suite, update)

import Common.Rest
import Expect
import Http
import Json.Decode as Decode
import Test exposing (Test, describe, test)
import Time
import TopStories
import Url.Builder as Url


stories : List TopStories.Story
stories =
    [ TopStories.Story (TopStories.StoryId "0") "Test" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"
    , TopStories.Story (TopStories.StoryId "1") "Test" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"
    ]


storyIds : List TopStories.StoryId
storyIds =
    List.map (\story -> story.id) stories


fullStoryString : String
fullStoryString =
    """{
    "by": "new_guy",
    "descendants": 11,
    "id": 18376089,
    "kids": [
        18376322,
        18376409,
        18376397,
        18376342,
        18376325,
        18376336,
        18376374,
        18376338
    ],
    "score": 65,
    "time": 1541342951,
    "title": "30 spies dead after Iran cracked CIA comms network with, er, Google search",
    "type": "story",
    "url": "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"
    }"""


suite : Test
suite =
    describe "TopStories"
        [ init
        , update
        , topStoryIdsDecoder
        , initCmd
        , getStoryUrl
        , storyDecoder
        , timestampDecoder
        , storyIdDecoder
        , addStoryDetails
        , loadStoryDetails
        ]


init : Test
init =
    describe "Init"
        [ test "sets init model to loading" <|
            \_ -> Expect.equal TopStories.init TopStories.Loading
        ]


update : Test
update =
    describe "Update"
        [ test "sets model to loading on load message" <|
            \_ ->
                let
                    model =
                        TopStories.Loaded []

                    msg =
                        TopStories.Load

                    modelExpected =
                        TopStories.Loading

                    commandExpected =
                        TopStories.getTopStoryIds
                in
                Expect.equal (TopStories.update msg model) ( modelExpected, commandExpected )
        , test "dispatches loading of story details" <|
            \_ ->
                let
                    model =
                        TopStories.Loading

                    msg =
                        TopStories.LoadedStoryIds (Result.Ok storyIds)

                    modelExpected =
                        TopStories.Loading

                    ( modelReturned, _ ) =
                        TopStories.update msg model
                in
                Expect.equal modelReturned modelExpected
        , test "sets error in case load fails" <|
            \_ ->
                let
                    model =
                        TopStories.Loading

                    error =
                        Http.NetworkError

                    msg =
                        TopStories.LoadedStoryIds (Result.Err error)

                    modelExpected =
                        TopStories.Failed error
                in
                Expect.equal (TopStories.update msg model) ( modelExpected, Cmd.none )
        , test "sets story on load success in case other stories already have been loaded" <|
            \_ ->
                let
                    model =
                        TopStories.Loaded stories

                    newStory =
                        TopStories.Story (TopStories.StoryId "3") "WithDetails" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"

                    msg =
                        TopStories.LoadedStory (Result.Ok newStory)

                    modelExpected =
                        TopStories.Loaded (newStory :: stories)
                in
                Expect.equal (TopStories.update msg model) ( modelExpected, Cmd.none )
        , test "sets story in case of first load" <|
            \_ ->
                let
                    model =
                        TopStories.Loading

                    newStory =
                        TopStories.Story (TopStories.StoryId "3") "WithDetails" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"

                    modelExpected =
                        TopStories.Loaded [ newStory ]

                    msg =
                        TopStories.LoadedStory (Result.Ok newStory)
                in
                Expect.equal (TopStories.update msg model) ( modelExpected, Cmd.none )
        , test "sets error in case story load fails" <|
            \_ ->
                let
                    model =
                        TopStories.Loading

                    error =
                        Http.NetworkError

                    msg =
                        TopStories.LoadedStory (Result.Err error)

                    modelExpected =
                        TopStories.Failed error
                in
                Expect.equal (TopStories.update msg model) ( modelExpected, Cmd.none )
        ]


loadStoryDetails : Test
loadStoryDetails =
    describe "loadStoryDetails"
        [ test "creates command for loading stories for given Ids" <|
            \_ ->
                let
                    cmdsExpected =
                        List.map (\storyId -> TopStories.getStory storyId) storyIds

                    cmdsReturned =
                        TopStories.loadStoryDetails storyIds
                in
                Expect.equal (List.length cmdsReturned) (List.length cmdsExpected)
        ]


addStoryDetails : Test
addStoryDetails =
    describe "addStoryDetails"
        [ test "adds story in case not existing" <|
            \_ ->
                let
                    newStory =
                        TopStories.Story (TopStories.StoryId "99") "Test" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"

                    storiesExpected =
                        newStory :: stories
                in
                Expect.equal (TopStories.addStoryDetails stories newStory) storiesExpected
        , test "replaces existing story" <|
            \_ ->
                let
                    newStory =
                        TopStories.Story (TopStories.StoryId "1") "Changed" (Time.millisToPosix 1541342951) 0 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"

                    storiesExpected =
                        List.head stories
                            |> Maybe.map List.singleton
                            |> Maybe.map ((::) newStory)
                            |> Maybe.withDefault stories
                in
                Expect.equal (TopStories.addStoryDetails stories newStory) storiesExpected
        ]


topStoryIdsDecoder : Test
topStoryIdsDecoder =
    describe "topStoryIdsDecoder"
        [ test "decodes list of story ids" <|
            \_ ->
                let
                    decodedStoryIds =
                        List.map (\story -> story.id) stories
                in
                Expect.equal ("[0, 1]" |> Decode.decodeString TopStories.topStoryIdsDecoder) (Result.Ok decodedStoryIds)
        ]


storyDecoder : Test
storyDecoder =
    describe "storyDecoder"
        [ test "decodes story" <|
            \_ ->
                let
                    storyString =
                        fullStoryString

                    storyExpected =
                        TopStories.Story (TopStories.StoryId "18376089") "30 spies dead after Iran cracked CIA comms network with, er, Google search" (Time.millisToPosix 1541342951) 65 "https://www.theregister.co.uk/2018/11/02/iran_cracked_cia_google/"
                in
                Expect.equal (storyString |> Decode.decodeString TopStories.storyDecoder) (Result.Ok storyExpected)
        ]


timestampDecoder : Test
timestampDecoder =
    describe "timestampDecoder"
        [ test "decodes timestamp" <|
            \_ ->
                let
                    timestampInMillis =
                        1541342951

                    timestampExpected =
                        Time.millisToPosix timestampInMillis
                in
                Expect.equal (String.fromInt timestampInMillis |> Decode.decodeString TopStories.timestampDecoder) (Result.Ok timestampExpected)
        ]


storyIdDecoder : Test
storyIdDecoder =
    describe "storyIdDecoder"
        [ test "decodes storyId" <|
            \_ ->
                let
                    storyId =
                        123456

                    storyIdExpected =
                        TopStories.StoryId (String.fromInt storyId)
                in
                Expect.equal (String.fromInt storyId |> Decode.decodeString TopStories.storyIdDecoder) (Result.Ok storyIdExpected)
        ]


getStoryUrl : Test
getStoryUrl =
    describe "getStoryUrl"
        [ test "returns url for story id" <|
            \_ ->
                let
                    storyIdPlain =
                        "1"

                    urlExpected =
                        Url.crossOrigin Common.Rest.baseUrlWithVersion [ TopStories.storyPath, storyIdPlain ++ ".json" ] []
                in
                Expect.equal (TopStories.getStoryUrl (TopStories.StoryId storyIdPlain)) urlExpected
        ]


initCmd : Test
initCmd =
    describe "InitCmd"
        [ test "dispatches loading of top stories" <|
            \_ ->
                Expect.equal TopStories.initCmd TopStories.getTopStoryIds
        ]
