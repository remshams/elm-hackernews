module Tests exposing (suite)

import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "First"
        [ test "Always true" <|
            \_ -> Expect.equal "test" "test"
        ]
