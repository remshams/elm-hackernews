const path = require('path');
const appPaths = require('./app_path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


module.exports = {

  output: {
    path: appPaths.dist,
    filename: 'hackernews.js'
  },

  resolve: {
    extensions: ['.elm', '.js'],
    modules: ['node_modules']
  },

  devServer: {
    historyApiFallback: true
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: path.join(appPaths.assets, 'index.html')
    })
  ]

};
